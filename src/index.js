const express = require('express');
const app = express();
const routes = require('./routes/index.js');
const path = require('path');
const bodyParser = require('body-parser');
const morgan = require('morgan');


//settings
app.set('port', process.env.port || 3000);
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');


//middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(morgan('dev'));


//routes
app.use(routes);


//start server
app.listen(app.get('port'), () => {
    console.log(`Server listening on port ${app.get('port')}`);
});