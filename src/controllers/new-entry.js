
let entries = [];


const newEntry = (req, res) => {
    res.render('new-entry', {title: 'New entry'});
};

const saveEntry = (req, res) => {
    if(!req.body.title || !req.body.content) {
        res.send(400).send('Las entradas deben tener un titulo y un contenido');
    }

    let newEntry = {
        title: req.body.title,
        content: req.body.content,
        published: new Date()
    };

    entries.push(newEntry);
    
    res.redirect('/');
};


module.exports = { newEntry, saveEntry, entries };