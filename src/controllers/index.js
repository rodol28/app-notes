const { entries } = require('./new-entry');

const index = (req, res) => {
    res.render('index', {title: 'App notes', notes: entries});
};

module.exports = { index };