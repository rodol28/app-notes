const express = require('express');
const router = express.Router();
const indexControllers = require('../controllers/index');
const newEntryControllers = require('../controllers/new-entry');

router.get('/', indexControllers.index);
router.get('/new-entry', newEntryControllers.newEntry);
router.post('/new-entry', newEntryControllers.saveEntry);

module.exports = router;